#!/usr/bin/env python

__author__ = "Andrea Vanzo"

#  Imports  #
# ROS imports
import rospy

# ROS msg and srv imports
from std_msgs.msg import String

# Python libraries
import aiml
import os


#  Classes  #
class AimlRosNode(object):
    # Variables #

    interpreter = aiml.Kernel()

    def __init__(self):
        # Initialize node #
        rospy.init_node('aiml_ros_node')

        # Reading params #
        self.aiml_path = rospy.get_param('~aiml_path', 'test')

        # Initialize publishers #
        self.aiml_publisher = rospy.Publisher('/dialogue_manager_response', String, queue_size=1)

        # Declare subscribers #
        rospy.Subscriber('/best_speech_hypothesis', String, self.aiml_callback, queue_size=1)

        self.__learn(self.aiml_path)
        print 'Waiting to serve you, Master!\n'
        rospy.spin()

    def aiml_callback(self, data):
        print 'Master said:\t' + data.data
        reply = self.interpreter.respond(data.data)
        print 'I say:\t\t' + reply + '\n'
        self.aiml_publisher.publish(reply)

    def __learn(self, path):
        for root, directories, file_names in os.walk(path):
            for filename in file_names:
                if filename.endswith('.aiml'):
                    print os.path.join(root, filename)
                    self.interpreter.learn(os.path.join(root, filename))
        print 'Number of categories: ' + str(self.interpreter.numCategories())


#  If Main  #
if __name__ == '__main__':
    AimlRosNode()
