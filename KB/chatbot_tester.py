import aiml
import os


def do_something(sentence):
    if '[SAY]' in sentence:
        print sentence.replace('[SAY]', '').strip()


def learn(interpreter, path):
    for root, directories, file_names in os.walk(path):
        for filename in file_names:
            if filename.endswith('.aiml'):
                interpreter.learn(os.path.join(root, filename))
    print 'Number of categories: ' + str(interpreter.numCategories())


k = aiml.Kernel()
learn(k, "it")

while 1:
    input_string = raw_input('Enter a sentence: ')
    reply = k.respond(input_string)
    do_something(reply)
